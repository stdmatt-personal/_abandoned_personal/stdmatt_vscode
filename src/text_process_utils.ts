//----------------------------------------------------------------------------//
//                        _      _                 _   _                      //
//                       | |    | |               | | | |                     //
//                    ___| |_ __| |_ __ ___   __ _| |_| |_                    //
//                   / __| __/ _` | '_ ` _ \ / _` | __| __|                   //
//                   \__ \ || (_| | | | | | | (_| | |_| |_                    //
//                   |___/\__\__,_|_| |_| |_|\__,_|\__|\__|                   //
//                                                                            //
//                                                                            //
//  File      : text_process_utils.ts                                         //
//  Project   : stdmatt-vscode                                                //
//  Date      : 04 Jan, 22                                                    //
//  License   : GPLv3                                                         //
//  Author    : stdmatt <stdmatt@pixelwizards.io>                             //
//  Copyright : stdmatt - 2022                                                //
//                                                                            //
//  Description :                                                             //
//                                                                            //
//---------------------------------------------------------------------------~//

//----------------------------------------------------------------------------//
// Imports                                                                    //
//----------------------------------------------------------------------------//
//------------------------------------------------------------------------------
import { JS_Utils }     from "./js_utils";
import { VS_Utils }     from "./vs_utils";
import { Comment_Info } from "./vs_utils";
import { Ruler_Info }   from "./vs_utils";


//----------------------------------------------------------------------------//
// Text Process Utils                                                         //
//----------------------------------------------------------------------------//
//------------------------------------------------------------------------------
export class Text_Process_Utils
{
    private static _KEYWORDS: {} | null = null;

    //--------------------------------------------------------------------------
    static templated_content(lines: string[]): string[]
    {
        Text_Process_Utils._generate_keywords();
        if(!Text_Process_Utils._KEYWORDS) {
            return [];
        }

        let output_lines:string[] = [];
        for(let line_index = 0; line_index < lines.length; ++line_index) {
            const content   = lines[line_index];
            let   templated = "";
            for(let i = 0; i < content.length; ++i) {
                const c = content[i];
                if(c != "$") {
                    templated += c;
                    continue;
                }

                const j     = Text_Process_Utils._get_first_non_token_index(content, i+1);
                const token = content.substring(i+1, j);
                const func  = Text_Process_Utils._KEYWORDS[token];

                if(!func) {
                    templated += ("$" + token);
                } else {
                    // @TODO(stdmatt): [LOG]
                    const value = func();
                    console.log("Replace token (", token, "): ", value);
                    templated += value;
                }
                i = (j-1); // increment of the for will bump it...;
            }

            templated += "\n";
            output_lines.push(templated);
        }

        return output_lines;
    }

    //--------------------------------------------------------------------------
    static comment_line(
        comment:        Comment_Info,
        ruler:          Ruler_Info,
        text:           string,
        current_column: number,
        force_non_line: boolean,
        symmetrical:    boolean,
        fill_char:      string ): string
    {
        const has_line_comment = (comment.line && comment.line.length != 0);

        let   start_comment = comment.start;
        let   end_comment   = comment.end;
        if(has_line_comment && !force_non_line) {
            start_comment = comment.line;
            end_comment   = (symmetrical) ? comment.line : "";
        }

        const clean_text   = JS_Utils.trim_right_newlines(text);
        const ruler_column = ruler.rulers[0];


        const should_repeat     = (fill_char.length > 0);
        const text_total_length = clean_text.length + (start_comment.length + end_comment.length);
        const repeat_count      = ruler_column - (current_column + text_total_length);

        // Already too big, but we need to comment it anyways....
        if(text_total_length > ruler_column) {
            return start_comment + " " + clean_text + " " + end_comment;
        }

        const commented_text = (should_repeat)
            ? clean_text + fill_char.repeat(repeat_count)
            : clean_text + " " .repeat(repeat_count);

        const final_text = (start_comment + commented_text + end_comment);
        return final_text;
    }

    //--------------------------------------------------------------------------
    private static _get_first_non_token_index(str: string, start: number): number
    {
        let index = start;
        while(true) {
            if(index >= str.length) {
                return index;
            }

            const c = str[index++];

            if(c == "$"            ) continue;
            if(c == "_"            ) continue;
            if(JS_Utils.is_alpha(c)) continue;
            if(JS_Utils.is_num  (c)) continue;

            return (index - 1);
        }
    }

    //--------------------------------------------------------------------------
    private static _generate_keywords(): void
    {
        if(Text_Process_Utils._KEYWORDS) {
            return;
        }

        const k = {};

        k["TM_SELECTED_TEXT"         ] = ()=>{ return VS_Utils.get_tm_selected_text        (); };
        k["TM_CURRENT_LINE"          ] = ()=>{ return VS_Utils.get_tm_current_line         (); };
        k["TM_CURRENT_WORD"          ] = ()=>{ return VS_Utils.get_tm_current_word         (); };
        k["TM_LINE_INDEX"            ] = ()=>{ return VS_Utils.get_tm_line_index           (); };
        k["TM_LINE_NUMBER"           ] = ()=>{ return VS_Utils.get_tm_line_number          (); };
        k["TM_FILENAME"              ] = ()=>{ return VS_Utils.get_tm_filename             (); };
        k["TM_FILENAME_BASE"         ] = ()=>{ return VS_Utils.get_tm_filename_base        (); };
        k["TM_DIRECTORY"             ] = ()=>{ return VS_Utils.get_tm_directory            (); };
        k["TM_FILEPATH"              ] = ()=>{ return VS_Utils.get_tm_filepath             (); };
        k["RELATIVE_FILEPATH"        ] = ()=>{ return VS_Utils.get_relative_filepath       (); };
        k["CLIPBOARD"                ] = ()=>{ return VS_Utils.get_clipboard               (); };
        k["WORKSPACE_NAME"           ] = ()=>{ return VS_Utils.get_workspace_name          (); };
        k["WORKSPACE_FOLDER"         ] = ()=>{ return VS_Utils.get_workspace_folder        (); };
        k["CURRENT_YEAR"             ] = ()=>{ return VS_Utils.get_current_year            (); };
        k["CURRENT_YEAR_SHORT"       ] = ()=>{ return VS_Utils.get_current_year_short      (); };
        k["CURRENT_MONTH"            ] = ()=>{ return VS_Utils.get_current_month           (); };
        k["CURRENT_MONTH_NAME"       ] = ()=>{ return VS_Utils.get_current_month_name      (); };
        k["CURRENT_MONTH_NAME_SHORT" ] = ()=>{ return VS_Utils.get_current_month_name_short(); };
        k["CURRENT_DATE"             ] = ()=>{ return VS_Utils.get_current_date            (); };
        k["CURRENT_DAY_NAME"         ] = ()=>{ return VS_Utils.get_current_day_name        (); };
        k["CURRENT_DAY_NAME_SHORT"   ] = ()=>{ return VS_Utils.get_current_day_name_short  (); };
        k["CURRENT_HOUR"             ] = ()=>{ return VS_Utils.get_current_hour            (); };
        k["CURRENT_MINUTE"           ] = ()=>{ return VS_Utils.get_current_minute          (); };
        k["CURRENT_SECOND"           ] = ()=>{ return VS_Utils.get_current_second          (); };
        k["CURRENT_SECONDS_UNIX"     ] = ()=>{ return VS_Utils.get_current_seconds_unix    (); };
        k["RANDOM"                   ] = ()=>{ return VS_Utils.get_random                  (); };
        k["RANDOM_HEX"               ] = ()=>{ return VS_Utils.get_random_hex              (); };
        k["UUID"                     ] = ()=>{ return VS_Utils.get_uuid                    (); };
        k["BLOCK_COMMENT_START"      ] = ()=>{ return VS_Utils.get_block_comment_start     (); };
        k["BLOCK_COMMENT_END"        ] = ()=>{ return VS_Utils.get_block_comment_end       (); };
        k["LINE_COMMENT"             ] = ()=>{ return VS_Utils.get_line_comment            (); };

        Text_Process_Utils._KEYWORDS = k;
    }
}