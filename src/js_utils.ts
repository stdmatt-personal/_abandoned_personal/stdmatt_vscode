//----------------------------------------------------------------------------//
//                        _      _                 _   _                      //
//                       | |    | |               | | | |                     //
//                    ___| |_ __| |_ __ ___   __ _| |_| |_                    //
//                   / __| __/ _` | '_ ` _ \ / _` | __| __|                   //
//                   \__ \ || (_| | | | | | | (_| | |_| |_                    //
//                   |___/\__\__,_|_| |_| |_|\__,_|\__|\__|                   //
//                                                                            //
//                                                                            //
//  File      : js_utils.ts                                                   //
//  Project   : stdmatt-vscode                                                //
//  Date      : 04 Jan, 22                                                    //
//  License   : GPLv3                                                         //
//  Author    : stdmatt <stdmatt@pixelwizards.io>                             //
//  Copyright : stdmatt - 2022                                                //
//                                                                            //
//  Description :                                                             //
//                                                                            //
//---------------------------------------------------------------------------~//

//----------------------------------------------------------------------------//
// JS Utils                                                                   //
//----------------------------------------------------------------------------//
//------------------------------------------------------------------------------
export class JS_Utils
{
    //--------------------------------------------------------------------------
    // @todo(stdmatt): We need to verify this names....
    static DEFAULT_STATIC_MEMBERS = ["name", "prototype", "length"];

    //--------------------------------------------------------------------------
    // @todo(stdmatt): [Check for non function members]???
    static get_static_members(class_, include_js_members = false)
    {
        let members = Object.getOwnPropertyNames(class_);
        if(include_js_members) {
            return members;
        }

        for(let i = 0; i < JS_Utils.DEFAULT_STATIC_MEMBERS.length; ++i) {
            const member_name = JS_Utils.DEFAULT_STATIC_MEMBERS[i];
            const index       = members.indexOf(member_name);

            if(index != -1) {
                members.splice(index, 1);
            }
        }

        return members;
    }


    //--------------------------------------------------------------------------
    static __try_catch(f)
    {
        try{
            return f();
        }
        catch(err)
        {
            JS_Utils.__maybe_debugger();
            return err;
        }
    }

    //--------------------------------------------------------------------------
    static __maybe_debugger()
    {
        debugger;
    }

    // @todo(stdmatt): [TRIM check if we don't find any thing...]
    static trim_right_newlines(text)
    {
        let end = -1;
        for(let i = (text.length -1); i >= 0; --i) {
            if(!JS_Utils.is_newline(text[i])) {
                end = i;
                break;
            }
        }

        const trimmed = text.substring(0, end+1);
        return trimmed;
    }

    //--------------------------------------------------------------------------
    static trim_whitespace_and_newline(text)
    {
        let start = -1;
        for(let i = 0; i < text.length; ++i) {
            if(!JS_Utils.is_whitespace(text[i])) {
                start = i;
                break;
            }
        }

        let end = -1;
        for(let i = (text.length -1); i >= 0; --i) {
            if(!JS_Utils.is_whitespace(text[i])) {
                end = i;
                break;
            }
        }

        const trimmed = text.substring(start, end+1);
        return trimmed;
    }

    //--------------------------------------------------------------------------
    static trim_end(original, to_remove)
    {
        const index = (original.length - to_remove.length);
        if(index < 0) {
            return original;
        }
        for(let i = 0; (i + index) < original.length; ++i) {
            const a = original [i + index];
            const b = to_remove[i];
            if(a != b) {
                return original;
            }
        }

        const final = original.substring(0, index);
        return final;
    }

    //--------------------------------------------------------------------------
    static is_alpha(c)
    {
        return c >= "a" && c <= "z"
            || c >= "A" && c <= "Z";
    }

    //--------------------------------------------------------------------------
    static is_num(c, test_hex = false)
    {
        const is_number = (c >= "0" && c <= "9");
        if(is_number) {
            return true;
        }

        const is_hex = (c >= "a" && c <= "f") || (c >= "A" && c <= "F");
        return (is_hex && test_hex);
    }

    //--------------------------------------------------------------------------
    static is_newline(c)
    {
        return (c == "\n");
    }

    //--------------------------------------------------------------------------
    static is_whitespace(c)
    {
        return (c == " " )
            || (c == "\n")
            || (c == "\t");
    }
}