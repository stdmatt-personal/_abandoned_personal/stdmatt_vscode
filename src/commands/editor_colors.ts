//----------------------------------------------------------------------------//
//                        _      _                 _   _                      //
//                       | |    | |               | | | |                     //
//                    ___| |_ __| |_ __ ___   __ _| |_| |_                    //
//                   / __| __/ _` | '_ ` _ \ / _` | __| __|                   //
//                   \__ \ || (_| | | | | | | (_| | |_| |_                    //
//                   |___/\__\__,_|_| |_| |_|\__,_|\__|\__|                   //
//                                                                            //
//                                                                            //
//  File      : editor_colors.ts                                              //
//  Project   : stdmatt-vscode                                                //
//  Date      : 04 Jan, 22                                                    //
//  License   : GPLv3                                                         //
//  Author    : stdmatt <stdmatt@pixelwizards.io>                             //
//  Copyright : stdmatt - 2022                                                //
//                                                                            //
//  Description :                                                             //
//                                                                            //
//---------------------------------------------------------------------------~//

//----------------------------------------------------------------------------//
// Imports                                                                    //
//----------------------------------------------------------------------------//
//------------------------------------------------------------------------------
import * as vscode from "vscode";
//------------------------------------------------------------------------------
import { VS_Utils }     from "../vs_utils";
import { Ruler_Info }   from "../vs_utils";
import { Comment_Info } from "../vs_utils";


//----------------------------------------------------------------------------//
// Constants                                                                  //
//----------------------------------------------------------------------------//
//------------------------------------------------------------------------------
const _ERROR_COLOR              = "#FF00FF";
const _EDITOR_COLOR_CONFIG_KEY  = "editor_colors";


//----------------------------------------------------------------------------//
// Editor Colors                                                              //
//----------------------------------------------------------------------------//
//------------------------------------------------------------------------------
export class Command_Editor_Colors
{
    //--------------------------------------------------------------------------
    static save_current_color_to_config(): void
    {
        const workspace_config = VS_Utils.get_config();
        if(!workspace_config) {
            return;
        }

        let editor_colors = workspace_config[_EDITOR_COLOR_CONFIG_KEY];
        editor_colors = (editor_colors) ? editor_colors : {};

        // @notice(stdmatt): [Object Cloning] - 20 Dec, 2021 at 12:19:53
        // editor_colors is a readonly object, so we need to iterate on it
        // and construct something that we can change the values to serialization.
        const editor_colors_new_obj = {};
        for(let k of Object.keys(editor_colors)) {
            editor_colors_new_obj[k] = editor_colors[k];
        }

        const workspace = VS_Utils.get_workspace();
        if(!workspace) {
            return;
        }

        const workspace_name = (workspace?.name ?? "");
        const hex_color      = _create_hex_color_from_workspace_name(workspace_name);
        editor_colors_new_obj[workspace_name] = hex_color;

        workspace_config
        .update(
            _EDITOR_COLOR_CONFIG_KEY,
            editor_colors_new_obj,
            vscode.ConfigurationTarget.Global
        )
        .then(
            (v)=>{
                // @todo(stdmatt): [LOG] - 21 Dec, 2021 at 09:52:29
                console.log("Saved...", v);
            },
            (e)=>{
                // @todo(stdmatt): [LOG] - 21 Dec, 2021 at 09:52:29
                console.log("Error...", e);
            }
        );
    }

    //--------------------------------------------------------------------------
    static set_editor_color(): void
    {
        const workspace = VS_Utils.get_workspace();
        if(!workspace) {
            return;
        }

        const command_name   = "peacock.enterColor";
        const workspace_name = workspace?.name ?? "";
        const desired_color  = _get_color_from_settings_or_create_one(workspace_name);

        console.log("Setting Editor Color: ", desired_color);
        vscode.commands.executeCommand(command_name, desired_color);
    }
}


//----------------------------------------------------------------------------//
// Private                                                                    //
//----------------------------------------------------------------------------//
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
function _is_valid_hex_color(str: string): boolean
{
    if(str[0] == "#" && str.length == 7) {
        for(let i = 1; i < str.length; ++i) {
            const c = str[i].toLowerCase();
            if((c < "0" || c > "9") && (c < "a" || c > "f")) {
                return false;
            }
        }
        return true;
    }
    return false;
}

//------------------------------------------------------------------------------
function _create_hex_color_from_workspace_name(name: string): string
{
    const name_hash = Math.abs(_str_to_hash(name));
    const hex       = name_hash.toString(16);
    let   clean_hex = "";

    // @todo(stdmatt): [Improve color selection] - 21 Dec, 2021 at 09:34:31
    if(hex.length < 6) {
        clean_hex = hex + "FF".repeat(6 - hex.length);
    } else if(hex.length > 6) {
        clean_hex = hex.substr(0, 6);
    }

    clean_hex = "#" + clean_hex;
    return clean_hex;
}

//------------------------------------------------------------------------------
function _get_color_from_settings_or_create_one(workspace_name: string): string
{
    let desired_color = _create_hex_color_from_workspace_name(workspace_name);

    // Get the Color from what user has configured.
    const workspace_config = VS_Utils.get_config();
    if(!workspace_config) {
        // @todo(stdmatt): [LOG] - 21 Dec, 2021 at 03:39:41
        return desired_color;
    }

    const editor_colors = workspace_config[_EDITOR_COLOR_CONFIG_KEY];
    if(!editor_colors[workspace_name]) {
        // @todo(stdmatt): [LOG] - 21 Dec, 2021 at 03:39:41
        return desired_color;
    }

    desired_color = editor_colors[workspace_name];
    if(!_is_valid_hex_color(desired_color)) {
        // @todo(stdmatt): [LOG] - 21 Dec, 2021 at 03:39:41
        desired_color = _ERROR_COLOR;
    }

    return desired_color;
}

//------------------------------------------------------------------------------
// @notice(stdmatt): [Hash Function] - 20 Dec, 2021 at 07:46:10
// got from: https://stackoverflow.com/a/7616484 - Thanks esmiralha
function _str_to_hash(str)
{
    var hash = 0, i, chr;
    if (str.length === 0) {
        return hash;
    }

    for (i = 0; i < str.length; i++) {
        chr   = str.charCodeAt(i);
        hash  = ((hash << 5) - hash) + chr;
        hash |= 0; // Convert to 32bit integer
    }
    return hash;
};