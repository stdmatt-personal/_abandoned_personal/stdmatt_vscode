//----------------------------------------------------------------------------//
//                        _      _                 _   _                      //
//                       | |    | |               | | | |                     //
//                    ___| |_ __| |_ __ ___   __ _| |_| |_                    //
//                   / __| __/ _` | '_ ` _ \ / _` | __| __|                   //
//                   \__ \ || (_| | | | | | | (_| | |_| |_                    //
//                   |___/\__\__,_|_| |_| |_|\__,_|\__|\__|                   //
//                                                                            //
//                                                                            //
//  File      : snippet_trigger_base.ts                                       //
//  Project   : stdmatt-vscode                                                //
//  Date      : 04 Jan, 22                                                    //
//  License   : GPLv3                                                         //
//  Author    : stdmatt <stdmatt@pixelwizards.io>                             //
//  Copyright : stdmatt - 2022                                                //
//                                                                            //
//  Description :                                                             //
//                                                                            //
//---------------------------------------------------------------------------~//

//----------------------------------------------------------------------------//
// Imports                                                                    //
//----------------------------------------------------------------------------//
//------------------------------------------------------------------------------
import * as vscode from "vscode";
//------------------------------------------------------------------------------
import { VS_Utils }           from "../vs_utils";
import { JS_Utils }           from "../js_utils";
import { Text_Process_Utils } from "../text_process_utils";


//----------------------------------------------------------------------------//
// Snippet Trigger Base                                                       //
//----------------------------------------------------------------------------//
export class Snippet_Trigger_Base
{
    static settings_key   = "";
    static leading_space  = false;
    static force_non_line = false;
    static symmetrical    = false;

    static register(): vscode.Disposable[]
    {
        const providers: vscode.Disposable[] = [];
        const config = VS_Utils.get_config();
        if(!config) {
            console.log("Can't get configuration");
            return providers;
        }

        const snippets = config[this.settings_key];
        if(!snippets) {
            console.log("Can't get configuration for key:", this.settings_key);
            return providers;
        }

        for(let key in snippets) {
            const provider = this._create_snippet(snippets, key);
            providers.push(provider);
        }

        return providers;
    }

    //--------------------------------------------------------------------------
    static _create_snippet(snippets: string[], key: string): vscode.Disposable
    {
        const snippet_trigger = key;
        console.log("Registering snippet - Trigger:", snippet_trigger);

        const self = this;
        const provider = vscode.languages.registerCompletionItemProvider(
            [{ pattern: '**', scheme: 'file' }, { pattern: '**', scheme: 'untitled' }],
            {
                // @XXX(stdmatt): [PERFORMANCE IMPROVEMENT]
                // We don't need to recalculate everything everytime that
                // user has pressed a key... improve it later...
                provideCompletionItems(_, __) {
                    const snippet_template   = snippets[key];
                    const templated_snippets = Text_Process_Utils.templated_content(snippet_template);

                    let commented_snippet = "";
                    for(let i = 0; i < templated_snippets.length; ++i) {
                        let template = (self.leading_space)
                            ? " " + templated_snippets[i]
                            :       templated_snippets[i];

                        commented_snippet += Text_Process_Utils.comment_line(
                            VS_Utils.get_current_comment(),
                            VS_Utils.get_ruler          (),
                            template,
                            0,
                            self.force_non_line,
                            self.symmetrical,
                            ""
                        );

                        commented_snippet += "\n";
                    }

                    const completion         = new vscode.CompletionItem(snippet_trigger);
                    completion.insertText    = new vscode.SnippetString (commented_snippet);
                    return [ completion ];
                }
            },
        );

        return provider;
    }
}