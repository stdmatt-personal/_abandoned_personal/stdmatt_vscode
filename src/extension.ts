//----------------------------------------------------------------------------//
//                        _      _                 _   _                      //
//                       | |    | |               | | | |                     //
//                    ___| |_ __| |_ __ ___   __ _| |_| |_                    //
//                   / __| __/ _` | '_ ` _ \ / _` | __| __|                   //
//                   \__ \ || (_| | | | | | | (_| | |_| |_                    //
//                   |___/\__\__,_|_| |_| |_|\__,_|\__|\__|                   //
//                                                                            //
//                                                                            //
//  File      : extension.ts                                                  //
//  Project   : stdmatt-vscode                                                //
//  Date      : 04 Jan, 22                                                    //
//  License   : GPLv3                                                         //
//  Author    : stdmatt <stdmatt@pixelwizards.io>                             //
//  Copyright : stdmatt - 2022                                                //
//                                                                            //
//  Description :                                                             //
//                                                                            //
//---------------------------------------------------------------------------~//

//----------------------------------------------------------------------------//
// Imports                                                                    //
//----------------------------------------------------------------------------//
//------------------------------------------------------------------------------
import * as vscode from "vscode";
import * as fs     from "fs";
import * as path   from "path";
//------------------------------------------------------------------------------
import { JS_Utils }           from "./js_utils";
import { VS_Utils }           from "./vs_utils";
import { Text_Process_Utils } from "./text_process_utils";
//------------------------------------------------------------------------------
import { Command_Comment_Line  } from "./commands/comment_line";
import { Command_Editor_Colors } from "./commands/editor_colors";
import { Command_Text_Process  } from "./commands/text_process";
//------------------------------------------------------------------------------
import { Snippet_General }     from "./snippet_triggers/snippets_triggers";
import { Snippet_File_Header } from "./snippet_triggers/snippets_triggers";


//----------------------------------------------------------------------------//
// Lifecycle                                                                  //
//----------------------------------------------------------------------------//
//------------------------------------------------------------------------------
export function activate(context: vscode.ExtensionContext)
{
    // Register Commands
    _register_command_class(context, Command_Comment_Line );
    _register_command_class(context, Command_Text_Process );
    _register_command_class(context, Command_Editor_Colors);

    // Register Snippets
    _register_snippet_class(context, Snippet_General);
    _register_snippet_class(context, Snippet_File_Header);

    //
    // Call every time that the extension gets activated.
    //
    Command_Editor_Colors.set_editor_color();
}

//------------------------------------------------------------------------------
export function deactivate() {}


//----------------------------------------------------------------------------//
// Private Functions                                                          //
//----------------------------------------------------------------------------//
//------------------------------------------------------------------------------
function _register_command_class(context: vscode.ExtensionContext, command_class: any): void
{
    // @notice(stdmatt): [Command Registration]
    // We consider all public static methods of a class a entry point
    // to the command, while ignoring all private functions with "_" prefix.
    const class_name   = command_class.name;
    const member_names = JS_Utils.get_static_members(command_class);
    for(let i = 0; i < member_names.length; ++i) {
        const member_name = member_names[i];
        if(member_name.startsWith("_")) {
            // @todo(stdmatt): [LOG]
            console.log("Skipping function:", class_name, member_name);
            continue;
        }

        const func            = command_class[member_name];
        const activation_name = (command_class.name + "_" + member_name);

        const fullname   = ("stdmatt_vscode." + activation_name);
        const disposable = vscode.commands.registerCommand(fullname, func);
        context.subscriptions.push(disposable);

        // @todo(stdmatt): [LOG] - 21 Dec, 2021 at 05:04:20
        console.log("Registering command:", fullname);
    }
}

//------------------------------------------------------------------------------
function _register_snippet_class(context: vscode.ExtensionContext, snippet_class: any): void
{
    const providers = snippet_class.register();
    for(let i = 0; i < providers.length; ++i) {
        const provider = providers[i];
        context.subscriptions.push(provider);
    }
}